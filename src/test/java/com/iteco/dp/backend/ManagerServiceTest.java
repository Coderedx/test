package com.iteco.dp.backend;

import com.iteco.dp.backend.entity.Manager;
import com.iteco.dp.backend.entity.Person;
import com.iteco.dp.backend.entity.Role;
import com.iteco.dp.backend.entity.User;
import com.iteco.dp.backend.enumerated.RoleType;
import com.iteco.dp.backend.service.ManagerService;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.HashSet;
import java.util.Random;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
@TestPropertySource(locations = "classpath:application.yml")
public class ManagerServiceTest {

    @Autowired
    private ManagerService managerService;

    @After
    public void tearDown() {
        managerService.deleteAll();
    }

    @Test
    public void createManager() throws Exception {
        @NotNull final Manager manager = getManager();
        Assert.assertEquals(manager.getId(), managerService.save(manager).getId());
    }

    @Test
    public void updateManager() throws Exception {
        @NotNull final Manager manager = getManager();
        managerService.save(manager);
        manager.getPerson().setFirstName("Updated");
        Assert.assertEquals(manager.getPerson().getFirstName(), managerService.save(manager).getPerson().getFirstName());
    }

    @Test(expected = Exception.class)
    public void deleteManager() throws Exception {
        @NotNull final Manager manager = getManager();
        managerService.save(manager);
        managerService.delete(manager.getId());
        managerService.findOne(manager.getId());
    }

    private Manager getManager() {
        @NotNull final Manager manager = new Manager();
        @NotNull final Person person = new Person();
        @NotNull final User user = new User();
        user.setPassword("ladksj");
        user.setLogin(String.valueOf(new Random().nextInt()));
        user.setRoles(new HashSet<>(Collections.singleton(new Role(user, RoleType.ADMIN))));
        person.setFirstName("firstName" + new Random().nextInt());
        person.setUser(user);
        manager.setPerson(person);
        return manager;
    }
}
