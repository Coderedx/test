package com.iteco.dp.backend;

import com.iteco.dp.backend.entity.Candidate;
import com.iteco.dp.backend.entity.Person;
import com.iteco.dp.backend.entity.Role;
import com.iteco.dp.backend.entity.User;
import com.iteco.dp.backend.enumerated.RoleType;
import com.iteco.dp.backend.service.CandidateService;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.HashSet;
import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
@TestPropertySource(locations = "classpath:application.yml")
public class CandidateServiceTest {

    @Autowired
    private CandidateService candidateService;

    @After
    public void tearDown() {
        candidateService.deleteAll();
    }

    @Test
    public void createCandidate() throws Exception {
        @NotNull final Candidate candidate = getCandidate();
        Assert.assertEquals(candidate.getId(), candidateService.save(candidate).getId());
    }

    @Test
    public void updateManager() throws Exception {
        @NotNull final Candidate candidate = getCandidate();
        candidateService.save(candidate);
        candidate.getPerson().setFirstName("Updated");
        Assert.assertEquals(candidate.getPerson().getFirstName(), candidateService.save(candidate).getPerson().getFirstName());
    }

    @Test(expected = Exception.class)
    public void deleteManager() throws Exception {
        @NotNull final Candidate candidate = getCandidate();
        candidateService.save(candidate);
        candidateService.delete(candidate.getId());
        candidateService.findOne(candidate.getId());
    }

    private Candidate getCandidate() {
        @NotNull final Candidate candidate = new Candidate();
        @NotNull final Person person = new Person();
        @NotNull final User user = new User();
        user.setPassword("ladksj");
        user.setLogin(String.valueOf(new Random().nextInt()));
        user.setRoles(new HashSet<>(Collections.singleton(new Role(user, RoleType.ADMIN))));
        person.setFirstName("firstName" + new Random().nextInt());
        person.setUser(user);
        candidate.setPerson(person);
        return candidate;
    }
}
