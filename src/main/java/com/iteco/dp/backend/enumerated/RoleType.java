package com.iteco.dp.backend.enumerated;

import org.jetbrains.annotations.NotNull;

public enum RoleType {

    ADMIN("Администратор"),
    TEACHER("Преподаватель"),
    CANDIDATE("Кандидат"),
    INTERN("Стажер");

    @NotNull private String name;
    RoleType(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String displayName() {
        return name;
    }
}
