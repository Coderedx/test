package com.iteco.dp.backend.service;

import com.iteco.dp.backend.entity.Person;
import com.iteco.dp.backend.repository.PersonRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Component
public class PersonService {

    @Autowired
    @NotNull
    private PersonRepository personRepository;

    @Transactional
    public void load(@Nullable final Person user) throws Exception {
        if (user == null || user.getId() == null)
            throw new Exception("Argument can't be empty or null");
        personRepository.saveAndFlush(user);
    }

    @Transactional
    public void load(@Nullable final List<Person> list) throws Exception {
        if (list == null)
            throw new Exception("Argument can't be empty or null");
        for (@NotNull final Person user : list)
            load(user);
    }

    public @NotNull Collection<Person> findAll() throws Exception {
        return personRepository.findAll();
    }

    @Nullable
    public Person findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (personRepository.findById(id).isPresent())
            return personRepository.findById(id).get();
        return null;
    }

    @Transactional
    public void merge(@Nullable final String id, @Nullable final Person user) throws Exception {
        if (user == null)
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        personRepository.saveAndFlush(user);
    }

    @Transactional
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        personRepository.deleteById(id);
    }

    @Transactional
    public void removeAll() throws Exception {
        personRepository.deleteAll();
    }

    public Person findOneByUserId(String userId) {
        return personRepository.findOneByUserId(userId);
    }
}