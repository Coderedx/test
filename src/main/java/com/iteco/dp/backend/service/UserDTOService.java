package com.iteco.dp.backend.service;

import com.iteco.dp.backend.dto.UserDTO;
import com.iteco.dp.backend.entity.User;
import com.iteco.dp.backend.repository.RoleRepository;
import com.iteco.dp.backend.repository.UserRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class UserDTOService {

    @Autowired
    @NotNull
    private UserRepository userRepository;

    @Autowired
    @NotNull
    private RoleRepository roleRepository;

    @Transactional
    public void load(@Nullable final UserDTO user) throws Exception {
        if (user == null || user.getId() == null || user.getLogin() == null)
            throw new Exception("Argument can't be empty or null");
        if (isLoginExist(user.getLogin()))
            throw new Exception("This login already exist!");
        final User userEntity = toEntity(user);
        userRepository.saveAndFlush(userEntity);
    }

    @Transactional
    public void load(@Nullable final List<UserDTO> list) throws Exception {
        if (list == null)
            throw new Exception("Argument can't be empty or null");
        for (@NotNull final UserDTO user : list)
            load(user);
    }

    public @NotNull Collection<UserDTO> findAll() throws Exception {
        @NotNull final List<UserDTO> list = new ArrayList<>();
        for (@NotNull final User user : userRepository.findAll())
            list.add(toDTO(user));
        return list;
    }

    @Nullable
    public UserDTO findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (userRepository.findById(id).isPresent())
            return toDTO(userRepository.findById(id).get());
        return null;
    }

    @Transactional
    public void merge(@Nullable final String id, @Nullable final UserDTO user) throws Exception {
        if (user == null || user.getLogin() == null)
            throw new Exception("Argument can't be empty or null");
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        User user1 = null;
        if (userRepository.findById(id).isPresent())
            user1 = userRepository.findById(id).get();
        if (user1 != null &&
                !user1.getLogin().equals(user.getLogin()) &&
                isLoginExist(user.getLogin()))
            throw new Exception("This login already exist!");
        if (user1 == null &&
                isLoginExist(user.getLogin()))
            throw new Exception("This login already exist!");
        user.setId(id);
        userRepository.saveAndFlush(toEntity(user));
    }

    @Transactional
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        userRepository.deleteById(id);
    }

    @Transactional
    public void removeAll() throws Exception {
        userRepository.deleteAll();
    }

    @Nullable
    public UserDTO findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty())
            return null;
        return toDTO(userRepository.findByLogin(login));
    }

    public boolean isPassCorrect(@Nullable final String login, @Nullable final String pass) throws Exception {
        if (login == null || login.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (pass == null || pass.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (!userRepository.existsByLogin(login))
            return false;
        @NotNull final User user = userRepository.findByLogin(login);
        if (user.getPassword().equals(pass))
            return true;
        return false;
    }

    public boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty())
            throw new Exception("Argument can't be empty or null");
        return userRepository.existsByLogin(login);
    }

    @Nullable
    private UserDTO toDTO(@Nullable final User user) {
        if (user == null)
            return null;
        @NotNull final UserDTO userDto = new UserDTO();
        if (user.getId() != null)
            userDto.setId(user.getId());
        if (user.getLogin() != null)
            userDto.setLogin(user.getLogin());
        return userDto;
    }

    @Nullable
    private User toEntity(@Nullable final UserDTO user) throws Exception {
        if (user == null)
            return null;
        @NotNull final User userEntity = new User();
        if (user.getId() != null)
            userEntity.setId(user.getId());
        if (user.getLogin() != null)
            userEntity.setLogin(user.getLogin());
        if (user.getPassword() == null || user.getPassword().isEmpty() || user.getPassword().length() < 6)
            throw new Exception("Pass can't be empty or less 6 symbols");
        userEntity.setPassword(user.getPassword());
        return userEntity;
    }
}