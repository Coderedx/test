package com.iteco.dp.backend.service;

import com.iteco.dp.backend.entity.Interview;
import com.iteco.dp.backend.repository.InterviewRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Component
public class InterviewService {

    @Autowired
    @NotNull
    private InterviewRepository interviewRepository;

    @Transactional
    public void merge(@Nullable final Interview manager) throws Exception {
        if (manager == null )
            throw new Exception("Argument can't be empty or null");

        interviewRepository.saveAndFlush(manager);
    }

    public @NotNull Collection<Interview> findAll() throws Exception {
        return interviewRepository.findAll();
    }

    @Nullable
    public Interview findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (interviewRepository.findById(id).isPresent())
            return interviewRepository.findById(id).get();
        return null;
    }

    @Transactional
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        interviewRepository.deleteById(id);
    }

    @Transactional
    public void removeAll() throws Exception {
        interviewRepository.deleteAll();
    }

    public Collection<Interview> findAllByCandidateId(String candidateId) {
        return interviewRepository.findAllByCandidateId(candidateId);
    }

    public Collection<Interview> findAllByTeacherId(String teacherId) {
        return interviewRepository.findAllByTeacherId(teacherId);
    }
}