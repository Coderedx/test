package com.iteco.dp.backend.service;

import com.iteco.dp.backend.entity.AbstractUser;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public abstract class AbstractUserService<T extends AbstractUser> {

    public abstract Collection<T> findAll();

    public abstract T save(T t) throws Exception;

    public abstract T findOne(String id) throws Exception;

    public abstract void delete(String id) throws Exception;

    public abstract void deleteAll();
}