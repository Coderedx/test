package com.iteco.dp.backend.service;

import com.iteco.dp.backend.entity.Manager;
import com.iteco.dp.backend.repository.ManagerRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Component
public class ManagerService extends AbstractUserService<Manager> {

    @NotNull
    @Autowired
    private ManagerRepository managerRepository;

    @Override
    public Collection<Manager> findAll() {
        return managerRepository.findAll();
    }

    @Override
    public Manager findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("Id can't by empty or null");
        return managerRepository.findById(id).orElseThrow(() -> new Exception("Entity not found"));
    }

    public Manager findOneByPersonId(String personId) throws Exception {
        return managerRepository.findOneByPersonId(personId).orElseThrow(() -> new Exception("Entity not found"));
    }

    @NotNull
    @Override
    @Transactional
    public Manager save(@Nullable final Manager manager) throws Exception {
        if (manager == null || manager.getPerson() == null || manager.getPerson().getUser() == null)
            throw new Exception("Argument can't be empty or null");
        return managerRepository.saveAndFlush(manager);
    }

    @Override
    public void delete(String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("Id can't by empty or null");
        managerRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        managerRepository.deleteAll();
    }
}