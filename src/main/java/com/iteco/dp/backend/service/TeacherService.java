package com.iteco.dp.backend.service;

import com.iteco.dp.backend.entity.Person;
import com.iteco.dp.backend.entity.Role;
import com.iteco.dp.backend.entity.Teacher;
import com.iteco.dp.backend.entity.User;
import com.iteco.dp.backend.enumerated.RoleType;
import com.iteco.dp.backend.repository.PersonRepository;
import com.iteco.dp.backend.repository.RoleRepository;
import com.iteco.dp.backend.repository.TeacherRepository;
import com.iteco.dp.backend.repository.UserRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Component
public class TeacherService {

    @Autowired
    @NotNull
    private TeacherRepository teacherRepository;

    @Autowired
    @NotNull
    private UserRepository userRepository;

    @Autowired
    @NotNull
    private RoleRepository roleRepository;

    @Autowired
    @NotNull
    private PersonRepository personRepository;

    @Transactional
    public void merge(@Nullable final Teacher manager) throws Exception {
        if (manager == null || manager.getPerson() == null)
            throw new Exception("Argument can't be empty or null");
        @NotNull final User user = manager.getPerson().getUser();
        @NotNull final Person person = manager.getPerson();

        if (user.getId() == null || user.getId().isEmpty())
            throw new Exception("Argument can't be empty or null");
        userRepository.saveAndFlush(user);
        if (roleRepository.findAllByUserId(user.getId()).isEmpty())
            roleRepository.saveAndFlush(new Role(user, RoleType.TEACHER));
        personRepository.saveAndFlush(person);
        teacherRepository.saveAndFlush(manager);
    }

    public @NotNull Collection<Teacher> findAll() throws Exception {
        return teacherRepository.findAll();
    }

    @Nullable
    public Teacher findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        if (teacherRepository.findById(id).isPresent())
            return teacherRepository.findById(id).get();
        return null;
    }

    @Transactional
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty())
            throw new Exception("Argument can't be empty or null");
        teacherRepository.deleteById(id);
    }

    @Transactional
    public void removeAll() throws Exception {
        teacherRepository.deleteAll();
    }

    public Teacher findOneByUserId(String userId) {
        return teacherRepository.findOneByPersonId(userId);
    }
}