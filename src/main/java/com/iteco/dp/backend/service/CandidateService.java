package com.iteco.dp.backend.service;

import com.iteco.dp.backend.entity.Candidate;
import com.iteco.dp.backend.repository.CandidateRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Component
public class CandidateService extends AbstractUserService<Candidate> {

    @NotNull
    @Autowired
    private CandidateRepository candidateRepository;

    @Override
    public Candidate findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("Id can't by empty or null");
        return candidateRepository.findById(id).orElseThrow(() -> new Exception("Entity not found"));
    }

    public Candidate findOneByPersonId(@Nullable final String personId) throws Exception {
        if(personId == null || personId.isEmpty()) throw new Exception("Id can't by empty or null");
        return candidateRepository.findByPersonId(personId).orElseThrow(() -> new Exception("Entity not found"));
    }

    @Override
    public Collection<Candidate> findAll() {
        return candidateRepository.findAll();
    }

    @NotNull
    @Override
    @Transactional
    public Candidate save(@Nullable final Candidate candidate) throws Exception {
        if (candidate == null || candidate.getPerson() == null || candidate.getPerson().getUser() == null)
            throw new Exception("Argument can't be empty or null");
        return candidateRepository.saveAndFlush(candidate);
    }

    @Override
    public void delete(String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("Id can't by empty or null");
        candidateRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        candidateRepository.deleteAll();
    }
}