package com.iteco.dp.backend.controller;

import com.iteco.dp.backend.dto.UserDTO;
import com.iteco.dp.backend.service.UserDTOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(value = "/user")
public class UserRestController {

    @Autowired
    private UserDTOService userService;

    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public ResponseEntity<Collection<UserDTO>> listUser() throws Exception {
        return ResponseEntity.ok(userService.findAll());
    }

    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    public ResponseEntity<UserDTO> getUser(@PathVariable final String id) throws Exception {
        return ResponseEntity.ok(userService.findOne(id));
    }

    @RequestMapping(value = "/mergeAll", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<UserDTO>> mergeAllUser(@RequestBody Collection<UserDTO> users) throws Exception {
        for (UserDTO user : users)
            userService.merge(user.getId(), user);
        return ResponseEntity.ok(userService.findAll());
    }

    @RequestMapping(value = "/merge", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> mergeUser(@RequestBody final UserDTO user) throws Exception {
        userService.merge(user.getId(), user);
        return ResponseEntity.ok(userService.findOne(user.getId()));
    }

    @RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
    public ResponseEntity removeUser(@PathVariable final String id) throws Exception {
        userService.remove(id);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/removeAll", method = RequestMethod.DELETE)
    public ResponseEntity removeAllUser() throws Exception {
        userService.removeAll();
        return ResponseEntity.ok().build();
    }
}
