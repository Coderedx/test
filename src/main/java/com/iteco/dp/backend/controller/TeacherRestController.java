package com.iteco.dp.backend.controller;

import com.iteco.dp.backend.entity.Person;
import com.iteco.dp.backend.entity.Teacher;
import com.iteco.dp.backend.entity.User;
import com.iteco.dp.backend.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(value = "/teacher")
public class TeacherRestController {

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private UserService userService;

    @Autowired
    private PersonService personService;

    @Autowired
    private InterviewService interviewService;

    @Autowired
    private CandidateService candidateService;

    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public ResponseEntity<Collection<Teacher>> listUser() throws Exception {
        return ResponseEntity.ok(teacherService.findAll());
    }

    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    public ResponseEntity<Teacher> getUser(@PathVariable final String id) throws Exception {
        return ResponseEntity.ok(teacherService.findOne(id));
    }

    @RequestMapping(value = "/merge", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Teacher> mergeUser(@RequestBody final Teacher user) throws Exception {
        teacherService.merge(user);
        return ResponseEntity.ok(teacherService.findOne(user.getId()));
    }

    @RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
    public ResponseEntity removeUser(@PathVariable final String id) throws Exception {
        teacherService.remove(id);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/removeAll", method = RequestMethod.DELETE)
    public ResponseEntity removeAllUser() throws Exception {
        teacherService.removeAll();
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<Teacher> register(@RequestHeader("login") String login,
                                              @RequestHeader("password") String password) throws Exception {
        if (userService.isLoginExist(login))
            throw new Exception("This login already exist!");
        final User user = new User();
        user.setLogin(login);
        userService.load(user);
        Person person = new Person();
        person.setUser(user);
        personService.load(person);
        Teacher candidate = new Teacher();
        candidate.setPerson(person);
        teacherService.merge(candidate);
        return ResponseEntity.ok(teacherService.findOne(candidate.getId()));
    }
}
