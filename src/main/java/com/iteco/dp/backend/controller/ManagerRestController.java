package com.iteco.dp.backend.controller;

import com.iteco.dp.backend.dto.ManagerDTO;
import com.iteco.dp.backend.dto.converter.DTOConverter;
import com.iteco.dp.backend.entity.Manager;
import com.iteco.dp.backend.entity.Role;
import com.iteco.dp.backend.enumerated.RoleType;
import com.iteco.dp.backend.service.ManagerService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/manager")
public class ManagerRestController {

    @Autowired
    private ManagerService managerService;

    @Autowired
    private DTOConverter dtoConverter;

    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public ResponseEntity<Collection<ManagerDTO>> findAll() throws Exception {
        return ResponseEntity.ok(managerService.findAll()
                .stream()
                .map(e -> dtoConverter.toManagerDTO(e))
                .collect(Collectors.toList())
        );
    }

    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    public ResponseEntity<ManagerDTO> findById(@PathVariable final String id) throws Exception {
        return ResponseEntity.ok(dtoConverter.toManagerDTO(managerService.findOne(id)));
    }

    @RequestMapping(value = "/create", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ManagerDTO> create(@RequestBody final ManagerDTO managerDTO) throws Exception {
        @NotNull final Manager manager = dtoConverter.toManagerEntity(managerDTO);
        manager.getPerson().getUser().setRoles(new HashSet<>(Collections.singletonList(new Role(manager.getPerson().getUser(), RoleType.CANDIDATE))));
        return ResponseEntity.ok(dtoConverter.toManagerDTO(managerService.save(manager)));
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ManagerDTO> update(@RequestBody final ManagerDTO manager) throws Exception {
        return ResponseEntity.ok(dtoConverter.toManagerDTO(managerService.save(dtoConverter.toManagerEntity(manager))));
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable final String id) throws Exception {
        managerService.delete(id);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/deleteAll", method = RequestMethod.DELETE)
    public ResponseEntity deleteAll() throws Exception {
        managerService.deleteAll();
        return ResponseEntity.ok().build();
    }
}
