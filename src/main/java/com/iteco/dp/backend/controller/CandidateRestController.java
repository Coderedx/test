package com.iteco.dp.backend.controller;

import com.iteco.dp.backend.dto.CandidateDTO;
import com.iteco.dp.backend.dto.converter.DTOConverter;
import com.iteco.dp.backend.entity.Candidate;
import com.iteco.dp.backend.entity.Role;
import com.iteco.dp.backend.enumerated.RoleType;
import com.iteco.dp.backend.service.CandidateService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/candidate")
public class CandidateRestController {

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private DTOConverter dtoConverter;

    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public ResponseEntity<Collection<CandidateDTO>> findAll() throws Exception {
        return ResponseEntity.ok(candidateService.findAll()
                .stream()
                .map(e -> dtoConverter.toCandidateDTO(e))
                .collect(Collectors.toList())
        );
    }

    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    public ResponseEntity<CandidateDTO> findById(@PathVariable final String id) throws Exception {
        return ResponseEntity.ok(dtoConverter.toCandidateDTO(candidateService.findOne(id)));
    }

    @RequestMapping(value = "/create", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CandidateDTO> create(@RequestBody final CandidateDTO candidateDTO) throws Exception {
        @NotNull final Candidate candidate = dtoConverter.toCandidateEntity(candidateDTO);
        candidate.getPerson().getUser().setRoles(new HashSet<>(Collections.singletonList(new Role(candidate.getPerson().getUser(), RoleType.CANDIDATE))));
        return ResponseEntity.ok(dtoConverter.toCandidateDTO(candidateService.save(candidate)));
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CandidateDTO> update(@RequestBody final CandidateDTO candidateDTO) throws Exception {
        return ResponseEntity.ok(dtoConverter.toCandidateDTO(candidateService.save(dtoConverter.toCandidateEntity(candidateDTO))));
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable final String id) throws Exception {
        candidateService.delete(id);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/deleteAll", method = RequestMethod.DELETE)
    public ResponseEntity deleteAll() throws Exception {
        candidateService.deleteAll();
        return ResponseEntity.ok().build();
    }
}
