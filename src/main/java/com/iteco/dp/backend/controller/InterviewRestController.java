package com.iteco.dp.backend.controller;

import com.iteco.dp.backend.entity.Interview;
import com.iteco.dp.backend.service.InterviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(value = "/interview")
public class InterviewRestController {

    @Autowired
    private InterviewService candidateService;

    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public ResponseEntity<Collection<Interview>> listUser() throws Exception {
        return ResponseEntity.ok(candidateService.findAll());
    }
   
    @RequestMapping(value = "/merge", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Interview> mergeUser(@RequestBody final Interview user) throws Exception {
        candidateService.merge(user);
        return ResponseEntity.ok(candidateService.findOne(user.getId()));
    }
    
    @RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
    public ResponseEntity removeUser(@PathVariable final String id) throws Exception {
        candidateService.remove(id);
        return ResponseEntity.ok().build();
    }
    
    @RequestMapping(value = "/removeAll", method = RequestMethod.DELETE)
    public ResponseEntity removeAllUser() throws Exception {
        candidateService.removeAll();
        return ResponseEntity.ok().build();
    }
}
