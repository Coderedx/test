package com.iteco.dp.backend.repository;

import com.iteco.dp.backend.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    boolean existsByLogin(String login);

    User findByLogin(String login);
}