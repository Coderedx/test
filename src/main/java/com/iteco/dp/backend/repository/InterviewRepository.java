package com.iteco.dp.backend.repository;

import com.iteco.dp.backend.entity.Interview;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface InterviewRepository extends JpaRepository<Interview, String> {

    Collection<Interview> findAllByCandidateId(String candidateId);

    Collection<Interview> findAllByTeacherId(String teacherId);
}