package com.iteco.dp.backend.repository;

import com.iteco.dp.backend.entity.Manager;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, String> {

    Optional<Manager> findOneByPersonId(@NotNull final String personId);
}