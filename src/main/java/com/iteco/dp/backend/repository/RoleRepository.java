package com.iteco.dp.backend.repository;

import com.iteco.dp.backend.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {

    Collection<Role> findAllByUserId(String userId);
}
