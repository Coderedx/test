package com.iteco.dp.backend.repository;

import com.iteco.dp.backend.entity.AbstractUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AbstractUserRepository<T extends AbstractUser> extends JpaRepository<T, String> {

}