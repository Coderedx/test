package com.iteco.dp.backend.repository;

import com.iteco.dp.backend.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, String> {

    Teacher findOneByPersonId(String userId);
}