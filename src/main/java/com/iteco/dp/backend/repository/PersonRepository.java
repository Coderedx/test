package com.iteco.dp.backend.repository;

import com.iteco.dp.backend.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<Person, String> {

    Person findOneByUserId(String userId);
}