package com.iteco.dp.backend.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractUser extends AbstractEntity {

    @Nullable
    @JoinColumn(name = "person_id")
    @OneToOne(cascade = CascadeType.ALL)
    private Person person;
}
