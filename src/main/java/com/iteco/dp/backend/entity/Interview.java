package com.iteco.dp.backend.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="app_interview")
public class Interview extends AbstractEntity{

    @Nullable
    @ManyToOne
    @JoinColumn(name = "teacher_id")
    private Teacher teacher;

    @Nullable
    @ManyToOne
    @JoinColumn(name = "candidate_id")
    private Candidate candidate;

    private Date date;
}
