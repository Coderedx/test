package com.iteco.dp.backend.entity;

import com.iteco.dp.backend.enumerated.RoleType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "app_roles")
public class Role extends AbstractEntity {

    @Nullable
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Enumerated(value = EnumType.STRING)
    private RoleType roleType;

    public Role(@Nullable User user, RoleType roleType) {
        this.user = user;
        this.roleType = roleType;
    }
}
