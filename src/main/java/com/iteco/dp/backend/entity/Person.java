package com.iteco.dp.backend.entity;

import com.iteco.dp.backend.enumerated.Sex;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="app_person")
public class Person extends AbstractEntity {

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private Date birthDate;

    @Nullable
    private String email;

    @Nullable
    private String phone;

    @Nullable
    private Sex sex;

    @Nullable
    @JoinColumn(name = "user_id")
    @OneToOne(cascade = CascadeType.ALL)
    private User user;
}
