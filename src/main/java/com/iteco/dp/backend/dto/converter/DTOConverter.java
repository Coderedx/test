package com.iteco.dp.backend.dto.converter;

import com.iteco.dp.backend.dto.*;
import com.iteco.dp.backend.entity.*;
import com.iteco.dp.backend.service.CandidateService;
import com.iteco.dp.backend.service.TeacherService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DTOConverter {

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private TeacherService teacherService;

    public User toUserEntity(@NotNull final UserDTO userDTO) throws Exception {
        @NotNull final User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPassword(userDTO.getPassword());
        return user;
    }

    public UserDTO toUserDTO(@NotNull final User user) {
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPassword(user.getPassword());
        return userDTO;
    }

    public Person toPersonEntity(@NotNull final PersonDTO personDTO) throws Exception {
        @NotNull final Person person = new Person();
        person.setId(personDTO.getId());
        person.setFirstName(personDTO.getFirstName());
        person.setLastName(personDTO.getLastName());
        person.setEmail(personDTO.getEmail());
        person.setPhone(personDTO.getPhone());
        person.setBirthDate(personDTO.getBirthDate());
        person.setSex(personDTO.getSex());
        person.setUser(toUserEntity(personDTO.getUserDTO()));
        return person;
    }

    public PersonDTO toPersonDTO(@Nullable final Person person) {
        if(person == null) return null;
        @NotNull final PersonDTO personDTO = new PersonDTO();
        personDTO.setId(personDTO.getId());
        personDTO.setFirstName(person.getFirstName());
        personDTO.setLastName(person.getLastName());
        personDTO.setEmail(person.getEmail());
        personDTO.setPhone(person.getPhone());
        personDTO.setBirthDate(person.getBirthDate());
        personDTO.setSex(person.getSex());
        personDTO.setUserDTO(toUserDTO(person.getUser()));
        return personDTO;
    }

    public ManagerDTO toManagerDTO(@NotNull final Manager manager) {
        @NotNull final ManagerDTO managerDTO = new ManagerDTO();
        managerDTO.setId(manager.getId());
        managerDTO.setPersonDTO(toPersonDTO(manager.getPerson()));
        return managerDTO;
    }

    public Manager toManagerEntity(@NotNull final ManagerDTO managerDTO) throws Exception {
        @NotNull final Manager manager = new Manager();
        manager.setId(managerDTO.getId());
        manager.setPerson(toPersonEntity(managerDTO.getPersonDTO()));
        return manager;
    }

    public Candidate toCandidateEntity(@Nullable final CandidateDTO candidateDTO) throws Exception {
        if(candidateDTO == null) return null;
        @NotNull final Candidate candidate = new Candidate();
        candidate.setId(candidateDTO.getId());
        candidate.setPerson(toPersonEntity(candidateDTO.getPersonDTO()));
        return candidate;
    }

    public CandidateDTO toCandidateDTO(@Nullable final Candidate candidate) {
        if(candidate == null) return null;
        @NotNull final CandidateDTO candidateDTO = new CandidateDTO();
        candidateDTO.setId(candidate.getId());
        candidateDTO.setPersonDTO(toPersonDTO(candidate.getPerson()));
        return candidateDTO;
    }

    public Interview toInterviewEntity(@Nullable final InterviewDTO interviewDTO) throws Exception {
        if(interviewDTO == null) return null;
        @NotNull final Interview interview = new Interview();
        interview.setId(interviewDTO.getId());
        interview.setCandidate(candidateService.findOne(interviewDTO.getCandidateId()));
        interview.setTeacher(teacherService.findOne(interviewDTO.getTeacherId()));
        interview.setDate(interviewDTO.getDate());
        return interview;
    }

    public InterviewDTO toInterviewDTO(@Nullable final Interview interview) {
        if(interview == null) return null;
        @NotNull final InterviewDTO interviewDTO = new InterviewDTO();
        interviewDTO.setId(interview.getId());
        interviewDTO.setTeacherId(interview.getTeacher().getId());
        interviewDTO.setCandidateId(interview.getCandidate().getId());
        interviewDTO.setDate(interview.getDate());
        return interviewDTO;
    }
}
