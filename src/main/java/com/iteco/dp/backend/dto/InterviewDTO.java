package com.iteco.dp.backend.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class InterviewDTO extends AbstractDTO {

    private String teacherId;

    private String candidateId;

    private Date date;
}
