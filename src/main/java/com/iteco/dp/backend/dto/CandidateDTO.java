package com.iteco.dp.backend.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CandidateDTO extends AbstractUserDTO {
}
